FROM --platform=$BUILDPLATFORM golang:1.15-buster as build

ARG TARGETARCH
ARG BUILDPLATFORM

WORKDIR /go/src/github.com/guildencrantz/docker-socks5
COPY . .
RUN go get && CGO_ENABLED=0 GOOS=linux GOARCH=$TARGETARCH go build -o socks5

FROM scratch
COPY --from=build /go/src/github.com/guildencrantz/docker-socks5/socks5 /socks5
ENTRYPOINT ["/socks5"]
